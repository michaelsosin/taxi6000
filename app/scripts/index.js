import { default as App } from './app'

const load = () => {
	const app = new App()
	app.init()
}

window.onload = load