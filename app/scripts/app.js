import { default as Overlay } from './order-overlay'

export default class App {
	constructor () {
		this.$container = document.getElementById('app')
		const orderOverlay = new Overlay()
	}

	init() {
		this.showApp()
	}

	showApp () {
		this.$container.classList.remove('loading')
	}
}

