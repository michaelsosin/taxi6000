export default class Overlay {
	constructor () {
		this.$element = document.getElementsByClassName('order-overlay')[0];
		this.$openButton = document.getElementById('order-taxi');
		this.$background = document.getElementsByClassName('overlay-background')[0];
		this.$closeButton = document.getElementsByClassName('overlay-close')[0];

		this.init();
	}

	init () {
		this.$openButton.addEventListener('click', this.open.bind(this));
		this.$background.addEventListener('click', this.close.bind(this));
		this.$closeButton.addEventListener('click', this.close.bind(this));
	}

	open () {
		this.$element.classList.add('opened')
		console.log('order opened')
	}

	close () {
		this.$element.classList.remove('opened')
	}
}