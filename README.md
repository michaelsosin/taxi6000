# Taxi 6000 Landing Page

Landing page for linking to Google Play and App Store application for taxi 6000

### Installation ###

* ```npm install -g gulp```
* ```npm install```

### Commands ###

* ```npm start (npm run build && gulp server && gulp watch)``` - start dev server
* ```npm run build (gulp clean && gulp build)``` - build bundle
