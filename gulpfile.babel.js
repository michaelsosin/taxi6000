'use strict';

import gulp from 'gulp';
import pug from 'gulp-pug';
import stylus from 'gulp-stylus';
import babel from 'gulp-babel';
import autoprefixer from 'gulp-autoprefixer';
import webserver from 'gulp-webserver';
import plumber from 'gulp-plumber';
import rimraf from 'gulp-rimraf';
import concat from 'gulp-concat';

const path = {
	srcDir: 'app/',
	styles: 'app/styles/**/*.{styl,css}',
	scripts: 'app/scripts/**/*.js',
	buildDir: 'build/'
};

gulp.task('clean', () => {
	return gulp.src(`${path.buildDir}/`, {read: false})
		.pipe(rimraf())
		.on('end', () => {
			console.log('Build directory cleaned')
		})
});

gulp.task('images', () => {
	gulp.src(`${path.srcDir}images/*`)
		.pipe(gulp.dest(`${path.buildDir}images`))
		.on('end', () => {
			console.log('Images moved')
		})
});

gulp.task('htaccess', () => {
	gulp.src(`${path.srcDir}.htaccess`)
		.pipe(gulp.dest(path.buildDir))
		.on('end', () => {
			console.log('.htaccess moved')
		})
});

gulp.task('pug', () => {
	gulp.src(`${path.srcDir}*.pug`)
		.pipe(plumber())
		.pipe(pug())
		.pipe(gulp.dest(path.buildDir))
		.on('end', () => {
			console.log('Pug to html done')
		})
});

gulp.task('stylus', () => {
	gulp.src(path.styles)
		.pipe(plumber())
		.pipe(stylus())
		.pipe(autoprefixer())
		.pipe(gulp.dest(`${path.buildDir}styles/`))
		.on('end', () => {
			console.log('Stylus to css done')
		})
});

gulp.task('babel', () => {
	gulp.src(path.scripts)
		.pipe(babel({
			presets: ['env'],
			plugins: ["transform-es2015-modules-umd"]
		}))
		.pipe(gulp.dest(`${path.buildDir}scripts/`))
		.on('end', () => {
			console.log('Babel to ES5 done')
		})
});

gulp.task('watch', () => {
	gulp.watch(`${path.srcDir}*.pug`, ['pug']);
	gulp.watch(path.styles, ['stylus']);
	gulp.watch(path.scripts, ['babel']);
});

gulp.task('server', () => {
	gulp.src(path.buildDir)
		.pipe(webserver({
			livereload: true,
			port: 3000
		}));
	gulp.start('watch');
});

gulp.task('build', ['images', 'htaccess', 'pug', 'stylus', 'babel']);
gulp.task('default', ['build']);
